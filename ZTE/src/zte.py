# ZTE ver2.0.12 (last update 17/3/15)

from physical import *
from System import TimeSpan
import struct

#######################################################################################################
#
#   Support functions
#
#######################################################################################################

def readRecord(buf):
    lst = []
    done = False
    lastByte = 0
    while not done:
        byte = buf.ReadByte()
        if buf.Position == buf.Length or byte == 10 and lastByte == 0:
            done = True
        else:
            lst.append(byte)
            lastByte = byte
                                    
    # Return the list as a string
    rec = ''.join(map(chr,lst))

    rec = rec.split('\0')           #       fields are null terminated
    rec.pop()                       # last field always blank, remove it
    return rec

def readVarint(buf):
    # Decode varint
    temp = value = buf.ReadByte()
    value = value & 0x7F
    j = 1
    while (temp & 0x80 and buf.Length - buf.Position):
        j += 1
        value = value << 7
        temp = buf.ReadByte()
        value += temp & 0x7F
    return value
				
def readString(buf, size=-1, typ="latin-1"):
    str = []        # Define an empty list

    if size==0: return ""
    elif size > 0: return buf.read(size).decode(typ)[:-1]
    else:
        if typ == "utf-16": bpc = 2
        else:bpc = 1
        
        while buf.Length - buf.Position:        
            char = buf.read(bpc).decode(typ)
            if char == "\x00": break
            str.append(char)

        # Return the list as a string
        return ''.join(str)

def readInt(buf,size=2):
    if size==2:
        return struct.unpack('<H', buf.read(2))[0]
    elif size==4:
        return struct.unpack('<I', buf.read(4))[0]
    elif size==8:
        return struct.unpack('<Q', buf.read(8))[0]


#######################################################################################################
#
#   ZTE SMS/MMS
#
#######################################################################################################

mms_standard_types = [
    "*/*",
    "text/*",
    "text/html",
    "text/plain",
    "text/x-hdml",
    "text/x-ttml",
    "text/x-vCalendar",
    "text/x-vCard",
    "text/vnd.wap.wml",
    "text/vnd.wap.wmlscript",
    "text/vnd.wap.wta-event",
    "multipart/*",
    "multipart/mixed",
    "multipart/form-data",
    "multipart/byterantes",
    "multipart/alternative",
    "application/*",
    "application/java-vm",
    "application/x-www-form-urlencoded",
    "application/x-hdmlc",
    "application/vnd.wap.wmlc",
    "application/vnd.wap.wmlscriptc",
    "application/vnd.wap.wta-eventc",
    "application/vnd.wap.uaprof",
    "application/vnd.wap.wtls-ca-certificate",
    "application/vnd.wap.wtls-user-certificate",
    "application/x-x509-ca-cert",
    "application/x-x509-user-cert",
    "image/*",
    "image/gif",
    "image/jpeg",
    "image/tiff",
    "image/png",
    "image/vnd.wap.wbmp",
    "application/vnd.wap.multipart.*",
    "application/vnd.wap.multipart.mixed",
    "application/vnd.wap.multipart.form-data",
    "application/vnd.wap.multipart.byteranges",
    "application/vnd.wap.multipart.alternative",
    "application/xml",
    "text/xml",
    "application/vnd.wap.wbxml",
    "application/x-x968-cross-cert",
    "application/x-x968-ca-cert",
    "application/x-x968-user-cert",
    "text/vnd.wap.si",
    "application/vnd.wap.sic",
    "text/vnd.wap.sl",
    "application/vnd.wap.slc",
    "text/vnd.wap.co",
    "application/vnd.wap.coc",
    "application/vnd.wap.multipart.related",
    "application/vnd.wap.sia",
    "text/vnd.wap.connectivity-xml",
    "application/vnd.wap.connectivity-wbxml",
    "application/pkcs7-mime",
    "application/vnd.wap.hashed-certificate",
    "application/vnd.wap.signed-certificate",
    "application/vnd.wap.cert-response",
    "application/xhtml+xml",
    "application/wml+xml",
    "text/css",
    "application/vnd.wap.mms-message",
    "application/vnd.wap.rollover-certificate",
    "application/vnd.wap.locc+wbxml",
    "application/vnd.wap.loc+xml",
    "application/vnd.syncml.dm+wbxml",
    "application/vnd.syncml.dm+xml",
    "application/vnd.syncml.notification",
    "application/vnd.wap.xhtml+xml",
    "application/vnd.wv.csp.cir",
    "application/vnd.oma.dd+xml",
    "application/vnd.oma.drm.message",
    "application/vnd.oma.drm.content",
    "application/vnd.oma.drm.rights+xml",
    "application/vnd.oma.drm.rights+wbxml"
    ]


def parseAttachment(pdufile, pdudata, message):
    attachment = Attachment()

    mms_item_header_len = pdudata.ReadByte()
    mms_item_data_len = readVarint(pdudata)
    mms_item_header_start = pdudata.Position
    mms_item_type_len = pdudata.ReadByte()
    mms_item_type_start = pdudata.Position
    mms_item_code = pdudata.ReadByte()
    mms_item_type = ""
    if mms_item_code < 0x80:                            # MMS type is a string
        pdudata.Position = pdudata.Position-1
        mms_item_type = readString(pdudata)
    elif mms_item_code < 0x80+len(mms_standard_types):  # MMS type is a standard code
        mms_item_type = mms_standard_types[mms_item_code-0x80]

    mms_item_type_end = pdudata.Position

    mms_item_name = ""
    mms_item_charset = ""

    while pdudata.Position < mms_item_header_start + mms_item_header_len:
        code = pdudata.ReadByte()
        if code == 0x81:
            mms_item_charset_pos = pdudata.Position-1
            charcode = pdudata.ReadByte()
            if charcode == 0xEA: mms_item_charset = "utf-8"
            elif charcode == 0x83: mms_item_charset = "ASCII"
        elif code == 0x86:
            mms_item_name_pos = pdudata.Position-1
            mms_item_name = readString(pdudata)
        elif code == 0x85 or code == 0x8E:
            if mms_item_name == "":
                mms_item_name_pos = pdudata.Position-1
                mms_item_name = readString(pdudata)

    # Assume that any TXT file is the message body - may not always be the case
    try:
        if mms_standard_types.index(mms_item_type) <= 10:
            pdudata.Position = mms_item_header_start + mms_item_header_len
            bodystart = pdudata.Position
            message.Body.Value = str(pdudata.read(mms_item_data_len))
            message.Body.Source = pdufile.Data.GetSubRange(bodystart, pdudata.Position)
    except:
        pass

    if mms_item_name !="":
        attachment.Filename.Value = mms_item_name
        attachment.Filename.Source = pdufile.Data.GetSubRange(mms_item_name_pos, len(mms_item_name)+1)
    if mms_item_type !="":
        attachment.ContentType.Value = mms_item_type
        attachment.ContentType.Source = pdufile.Data.GetSubRange(mms_item_type_start, mms_item_type_end - mms_item_type_start)
    if mms_item_charset !="":
        attachment.Charset.Value = mms_item_charset
        attachment.Charset.Source = pdufile.Data.GetSubRange(mms_item_charset_pos, len(mms_item_charset)+1)
    
    attachment.Data.Source = pdufile.Data.GetSubRange(mms_item_header_start + mms_item_header_len,mms_item_data_len)
    pdudata.Position = mms_item_header_start + mms_item_header_len + mms_item_data_len
    message.Attachments.Add(attachment)


def parseMMS(pdufile, pdudata, message):

    pdudata.seek(0)

    # An MMS file consists of a number of fields beginning with a field ID byte
    # which is from 0x80 to 0x9D.  It is assumed that fields can be in any order.

    # Keep looking for fields until the end of the file
    while pdudata.Length-pdudata.Position>0:
        fieldtype = pdudata.ReadByte()
        if fieldtype == 0x8C:
            mms_msg_type = pdudata.ReadByte()
        elif fieldtype == 0x98:
            mms_trans_id = readString(pdudata)
        elif fieldtype == 0x8D:
            mms_ver = pdudata.ReadByte()
        elif fieldtype == 0x8B:
            mms_msg_id = readString(pdudata)
        elif fieldtype == 0x85:
            mms_date = pdudata.read(5)
        elif fieldtype == 0x89:
            mms_fromlen = pdudata.ReadByte()
            if pdudata.ReadByte() == 0x80:
                mms_from_pos = pdudata.Position
                mms_from = readString(pdudata).split("/")[0]
                if not message.From.Value or len(message.From.Value.Identifier.Value) == 0 or message.From.Value.Identifier.Value[0] == "-":
                    message.From.Value = Party.MakeFrom(mms_from,pdufile.Data.GetSubRange(mms_from_pos,len(mms_from)))
        elif fieldtype == 0x97:
            mms_tolen = pdudata.ReadByte()
            pdudata.ReadByte()
            mms_to_pos = pdudata.Position
            mms_to = readString(pdudata).split("/")[0]  # TODO: handle multiple TO parties
            if len(message.To) == 0:
                message.To.Add(Party.MakeTo(mms_to,pdufile.Data.GetSubRange(mms_to_pos,len(mms_to))))
        elif fieldtype == 0x8A:
            mms_msg_class = pdudata.ReadByte()
        elif fieldtype == 0x8F:
            mms_priority = pdudata.ReadByte() - 0x80
            if mms_priority == 0:  message.Priority.Value = MailPriority.Low
            elif mms_priority == 2:  message.Priority.Value = MailPriority.High
            else:  message.Priority.Value = MailPriority.Normal
            message.Priority.Source = pdufile.Data.GetSubRange(pdudata.Position-2,2)
        elif fieldtype == 0x86:
            mms_delivery_report = pdudata.ReadByte()
        elif fieldtype == 0x90:
            mms_read_report = pdudata.ReadByte()
        elif fieldtype == 0x84:  #  Content field contains most of the useful data including attachments
            mms_content_type_len = pdudata.ReadByte()
            mms_content_type = pdudata.read(mms_content_type_len)
            mms_content_count = pdudata.ReadByte()
            for i in range(mms_content_count):
                parseAttachment(pdufile, pdudata, message)
    return 

def parsePush(pdufile, pdudata, message):
    pdudata.seek(0)

    # Read through the file looking for the byte '0xC6', which indicates the start of
    # the 'indication' tag
    while pdudata.Length-pdudata.Position>0:
        if pdudata.ReadByte() == 0xC6:
            # Now look for the byte '0x01', which indicates the end of
            # the 'indication' attributes, immediately after which is the message
            while pdudata.Length-pdudata.Position>0:
                if pdudata.ReadByte() == 0x01:
                    # First byte should be 0x03 to indicate an inline string
                    if pdudata.ReadByte() == 0x03:
                        bodystart = pdudata.Position
                        message.Body.Value = readString(pdudata)
                        message.Body.Source = pdufile.Data.GetSubRange(bodystart, pdudata.Position)
                    return
                
def parsePDU(pdufilename, messageType, message):
    pdufile = ds.FileSystems[0].TryGetByPath(pdufilename)[1]

    if (pdufile): pdudata = pdufile.Data
    else: return

    pdudata.seek(0)
    
    if messageType==0 or messageType==2:
        parseMMS(pdufile, pdudata, message)
    elif  messageType==3:
        readString(pdudata)
        readString(pdudata)
        body_pos = pdudata.Position
        message.Body.Value = readString(pdudata)
        message.Body.Source = pdufile.Data.GetSubRange(body_pos,len(message.Body.Value))
        
    else:           # messageType==1:
        # read the SMSC if it is not a sent message
        if message.Status.Value==MessageStatus.Read or message.Status.Value==MessageStatus.Unread:
            # Could be a SMSC or the from party.
            message.SMSC.Value = readString(pdudata)
            message.SMSC.Source = pdufile.Data.GetSubRange(0,pdudata.Position)

            # Skip a heap of nuls and unknown data
            pdudata.Position += 11

        # Read the message body
        body_pos = pdudata.Position
        message.Body.Value = readString(pdudata)
        message.Body.Source = pdufile.Data.GetSubRange(body_pos,len(message.Body.Value))


def parseIND(indfile, inddata, folder, root):
		
    fields = readRecord(inddata)       #       read a whole record
    errorcopy = fields[:]

    position = inddata.Position - 1

    if len(fields) != 8: return  # Dud record

    time = fields.pop()
    timestamp_len = (len(time)+1)
    position = position - timestamp_len
    timestamp_pos = position
    time = time.split(":")

    try:
        timeStamp = TimeStamp(DateTime(int(time[0]),int(time[1]),int(time[2]),int(time[3]),int(time[4]),int(time[5])))

        pdufilename = fields.pop()[3:]
        position = position - (len(pdufilename) + 4)

        partbody = fields.pop()
        position = position - (len(partbody) + 1)
        
        toparty = fields.pop()
        position = position - (len(toparty) + 1)
        toparty_pos = position
        toparty = toparty.split(";")
        
        fromparty = fields.pop()
        position = position - (len(fromparty) + 1)
        fromparty_pos = position

        messageStatus = int(fields.pop())
        position = position - 2
        messageStatus_pos = position
        
        messageType = int(fields.pop())
        id = fields.pop()

    except:
        # 7 record field - missing field will be either the body or the id
        time = fields.pop()
        timestamp_len = (len(time)+1)
        position = position - timestamp_len
        timestamp_pos = position
        time = time.split(":")
        
        try:
            timeStamp = TimeStamp(DateTime(int(time[0]),int(time[1]),int(time[2]),int(time[3]),int(time[4]),int(time[5])))
        except:
            print "Error: No timestamp"
            print str(errorcopy)
            return

        pdufilename = fields.pop()[3:]
        position = position - (len(pdufilename) + 4)

        # There will be missing field will either be the body or the ID
        # The message type <=3 so if the first field is >3 assume it is the ID and the body is missing (ID<4 could cause problem)
        if int(fields[0])>3:
            partbody = ""

            toparty = fields.pop()
            position = position - (len(toparty) + 1)
            toparty_pos = position
            toparty = toparty.split(";")

            fromparty = fields.pop()
            position = position - (len(fromparty) + 1)
            fromparty_pos = position

            messageStatus = int(fields.pop())
            position = position - 2
            messageStatus_pos = position

            messageType = int(fields.pop())
            id = fields.pop()

        # ID is missing
        else:
            partbody = fields.pop()
            position = position - (len(partbody) + 1)

            toparty = fields.pop()
            position = position - (len(toparty) + 1)
            toparty_pos = position
            toparty = toparty.split(";")

            fromparty = fields.pop()
            position = position - (len(fromparty) + 1)
            fromparty_pos = position

            messageStatus = int(fields.pop())
            position = position - 2
            messageStatus_pos = position

            messageType = int(fields.pop())
            id = "0"
                                              
    if (messageType==0):
        message = MMS()
    elif (messageType==1):
        message = SMS()
    elif (messageType==2):
        message = MMS()
    elif (messageType==3):
        message = SMS()                 #       WAP PUSH
    else:
        print "Error:  Unknown message type '" + str(messageType) + "' in message:"
        print str(errorcopy)
        return                          #       Error

    message.Deleted = DeletedState.Intact
    message.Folder.Value = folder
    message.TimeStamp.Value = timeStamp
    message.TimeStamp.Source = indfile.Data.GetSubRange(timestamp_pos,timestamp_len)

    if(messageStatus==0):
        message.Status.Value = MessageStatus.Read
    elif(messageStatus==1):
        message.Status.Value = MessageStatus.Unread
    elif(messageStatus==2):
        message.Status.Value = MessageStatus.Sent
    elif(messageStatus==3 or messageStatus==4):
        message.Status.Value = MessageStatus.Unsent
    else:
        print "Error:  Unknown message status '" + str(messageStatus) + "' in message:"
        print str(errorcopy)
        return                          #       Error
    message.Status.Source = indfile.Data.GetSubRange(messageStatus_pos,2)

    if (messageType==1 or messageType==3):
        if (len(fromparty) > 0):
            message.Parties.Add(Party.MakeFrom(fromparty,indfile.Data.GetSubRange(fromparty_pos,len(fromparty)+1)))

        if (len(toparty[0]) > 0):
            for i in range(len(toparty)):
                if len(toparty[i])>1 and ord(toparty[i][1])==0xFF: toparty[i]=toparty[i][3:]
                if len(toparty[i])>0: message.Parties.Add(Party.MakeTo(toparty[i],indfile.Data.GetSubRange(toparty_pos,len(toparty[i])+1)))
                toparty_pos = toparty_pos + len(toparty[i]) + 1
    elif (messageType==0 or messageType==2):
        if (len(fromparty) > 0):
            message.From.Value = Party.MakeFrom(fromparty,indfile.Data.GetSubRange(fromparty_pos,len(fromparty)+1))

        # Add the to parties
        if(len(toparty[0]) > 0):
            for i in range(len(toparty)):
                if ord(toparty[i][1])==0xFF: toparty[i]=toparty[i][3:]
                message.To.Add(Party.MakeTo(toparty[i],indfile.Data.GetSubRange(toparty_pos,len(toparty[i])+1)))
                toparty_pos = toparty_pos + len(toparty[i]) + 1

    parsePDU(root + pdufilename, messageType, message)

    # Add the message to Cellebrite
    ds.Models.Add(message)
    print message


def findMessages():

    # List of IND file names that we know about
    files = ["in.ind",  "out.ind", "sent.ind", "draft.ind"]

    # Friendly names for these folders
    folders = ["Inbox", "Outbox", "Sent", "Drafts"]

    fs = ds.FileSystems[0]
    for f in fs.Search('/mod/umbapp$'):
        storepath = f.AbsolutePath + "/"
        storeroot = storepath[:-12]
        print "Parsing messages store: " + storepath

        # Path to the IND files
        confpath = "umb/conf/"

        # Userbox config file
        userboxfilename = "umbuserfolderprofile_en"

        # Get the user folder names
        userboxfile = ds.FileSystems[0].TryGetByPath(storepath+userboxfilename)[1]
        if (userboxfile):
            userboxdata = userboxfile.Data

            # Get all user folder names
            for i in range(userboxdata.Length/0x3E):        #  Each record is 0x3E long
                userboxdata.Position = i*0x3E+2             #  Name is a UTF-16 string starting 2 bytes in
                files.append("userbox"+str(i+1)+".ind")
                folders.append(readString(userboxdata,-1,"utf-16"))

        # Parse each IND file in turn
        for i in range(len(files)):
            indfile = ds.FileSystems[0].TryGetByPath(storepath+confpath+files[i])[1]
            if (indfile):
                inddata = indfile.Data
                inddata.seek(0)

                # While we are not at the end of the file continue to get messages
                while inddata.Length - inddata.Position:
                    parseIND(indfile, inddata, folders[i], storeroot)


#######################################################################################################
#
#   ZTE CONTACT
#
#######################################################################################################
#
# Contact Structure:
# - Record size (16bitLE)
# - ID (16bitLE)
# - Unknown (16bitLE)
# - Unknown (16bitLE)
# - Field type (16bitLE):
#       - 0x0001: First name
#       - 0x0003: Last name
#       - 0x0005: Name
#       - 0x000F: Email address (public)
#       - 0xFF1C: Email address (other)
#       - 0x0011: Work number
#       - 0x0012: Home number
#       - 0x0013: Fax number
#       - 0x0018: Mobile number (public)
#       - 0xFF53: Mobile number (other)
#       - 0xFF0F: Street
#       - 0x0009: City
#       - 0x000A: State
#       - 0x000B: Country
#       - 0x000C: Postcode
#       - 0xFF3D: Unknown
#       - 0xFF01: Unknown
# - Field encoding (32bitLE):
#       - 0x00000002: Enum/Bool
#       - 0x00000005: Integer
#       - 0x00000009: UTF-16
#       - 0x00000006: latin-1
# - Size of field in bytes (16bitLE)
# - Field data
# - ... more fields

contacttypes = [
        "",
        "Unclassified",
        "Family",
        "Friends",
        "Work",
        "VIP",
        "",
        "",
        "Built-in"]

def parseContact(contactfile, contactdata, deletedcontacts):

    cont = Contact ()

    recordstart = contactdata.Position

    recordsize = readInt(contactdata)

    recordend = recordstart + recordsize

    if recordend > contactdata.Length: recordend = contactdata.Length
    
    recordid = readInt(contactdata)

    if recordid in deletedcontacts:
        cont.Deleted = DeletedState.Deleted
    else:
        cont.Deleted = DeletedState.Intact
    
    readInt(contactdata)

    cont.Group.Source = contactfile.Data.GetSubRange(contactdata.Position, 2)
    contacttype = readInt(contactdata)
    if contacttype > 8: contacttype=0
    cont.Group.Value = contacttypes[contacttype]

    namestart = contactdata.Length
    nameend = 0
    firstname = ""
    lastname = ""

    while contactdata.Position < recordend:
        fieldType = readInt(contactdata)

        if fieldType == 0x0001:  # First name
            if contactdata.Position - 2 < namestart: namestart = contactdata.Position - 2
            encoding = readInt(contactdata, 4)
            if encoding == 0x09: firstname = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: firstname = readString(contactdata,readInt(contactdata,2))
            if contactdata.Position > nameend: nameend = contactdata.Position
            
        elif fieldType == 0x0003:  # Last name
            if contactdata.Position - 2 < namestart: namestart = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            if encoding == 0x09: lastname = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: lastname = readString(contactdata,readInt(contactdata,2))
            if contactdata.Position > nameend: nameend = contactdata.Position
            
        elif fieldType == 0x0005:  # Name
            namestart = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            if encoding == 0x09: lastname = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: lastname = readString(contactdata,readInt(contactdata,2))
            nameend = contactdata.Position

        elif fieldType == 0x000F or fieldType == 0xFF1C:  # Email address
            emailpos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            email = EmailAddress()
            email.Deleted = cont.Deleted
            if encoding == 0x09: email.Value.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: email.Value.Value = readString(contactdata,readInt(contactdata,2))
            email.Value.Source = contactfile.Data.GetSubRange(emailpos, contactdata.Position-emailpos)
            cont.Entries.Add(email)

        elif fieldType == 0x0011:  # Work number
            homepos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            ph = PhoneNumber()
            ph.Deleted = cont.Deleted
            ph.Category.Value = "Work"
            if encoding == 0x09: ph.Value.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: ph.Value.Value = readString(contactdata,readInt(contactdata,2))
            ph.Value.Source = contactfile.Data.GetSubRange(homepos, contactdata.Position-homepos)
            cont.Entries.Add(ph)

        elif fieldType == 0x0012:  # Home number
            homepos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            ph = PhoneNumber()
            ph.Deleted = cont.Deleted
            ph.Category.Value = "Home"
            if encoding == 0x09: ph.Value.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: ph.Value.Value = readString(contactdata,readInt(contactdata,2))
            ph.Value.Source = contactfile.Data.GetSubRange(homepos, contactdata.Position-homepos)
            cont.Entries.Add(ph)

        elif fieldType == 0x0013:  # Fax number
            homepos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            ph = PhoneNumber()
            ph.Deleted = cont.Deleted
            ph.Category.Value = "Fax"
            if encoding == 0x09: ph.Value.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: ph.Value.Value = readString(contactdata,readInt(contactdata,2))
            ph.Value.Source = contactfile.Data.GetSubRange(homepos, contactdata.Position-homepos)
            cont.Entries.Add(ph)

        elif fieldType == 0x0018 or fieldType == 0xFF53:  # Mobile number (public or other)
            mobilepos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            ph = PhoneNumber()
            ph.Deleted = cont.Deleted
            ph.Category.Value = "Mobile"
            if encoding == 0x09: ph.Value.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: ph.Value.Value = readString(contactdata,readInt(contactdata,2))
            ph.Value.Source = contactfile.Data.GetSubRange(mobilepos, contactdata.Position-mobilepos)
            cont.Entries.Add(ph)

        elif fieldType == 0xFF0F or fieldType == 0xFF59:  # Street
            addresspos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            if not 'address' in locals():
                address = StreetAddress()
                address.Deleted = cont.Deleted
            if encoding == 0x09: address.Street1.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: address.Street1.Value = readString(contactdata,readInt(contactdata,2))
            address.Street1.Source = contactfile.Data.GetSubRange(addresspos, contactdata.Position-addresspos)

        elif fieldType == 0x0009:  # City
            addresspos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            if not 'address' in locals():
                address = StreetAddress()
                address.Deleted = cont.Deleted
            if encoding == 0x09: address.City.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: address.City.Value = readString(contactdata,readInt(contactdata,2))
            address.City.Source = contactfile.Data.GetSubRange(addresspos, contactdata.Position-addresspos)

        elif fieldType == 0x000A:  # State
            addresspos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            if not 'address' in locals():
                address = StreetAddress()
                address.Deleted = cont.Deleted
            if encoding == 0x09: address.State.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: address.State.Value = readString(contactdata,readInt(contactdata,2))
            address.State.Source = contactfile.Data.GetSubRange(addresspos, contactdata.Position-addresspos)

        elif fieldType == 0x000B:  # Country
            addresspos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            if not 'address' in locals():
                address = StreetAddress()
                address.Deleted = cont.Deleted
            if encoding == 0x09: address.Country.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: address.Country.Value = readString(contactdata,readInt(contactdata,2))
            address.Country.Source = contactfile.Data.GetSubRange(addresspos, contactdata.Position-addresspos)

        elif fieldType == 0x000C:  # Postcode
            addresspos = contactdata.Position - 2
            encoding = readInt(contactdata,4)
            if not 'address' in locals():
                address = StreetAddress()
                address.Deleted = cont.Deleted
            if encoding == 0x09: address.PostalCode.Value = readString(contactdata,readInt(contactdata,2), "utf-16")
            elif encoding == 0x06: address.PostalCode.Value = readString(contactdata,readInt(contactdata,2))
            address.PostalCode.Source = contactfile.Data.GetSubRange(addresspos, contactdata.Position-addresspos)

        elif fieldType == 0xFF01:  # Unknown field
            encoding = readInt(contactdata,4)
            val = readInt(contactdata, readInt(contactdata, 2))

        elif fieldType == 0xFF3D:  # Unknown field
            encoding = readInt(contactdata,4)
            val = readInt(contactdata, readInt(contactdata, 2))

        else:
            print "Unknown or bad field type: " + hex(fieldType) + " in contact beginning at offset: " + hex(recordstart)
            encoding = readInt(contactdata,4)
            val = contactdata.read(readInt(contactdata, 2))

    if len(firstname) > 0 and len(lastname) > 0:
        cont.Name.Value = firstname+" "+lastname
    else: cont.Name.Value = firstname+lastname
    
    cont.Name.Source = contactfile.Data.GetSubRange(namestart, nameend-namestart)

    if 'address' in locals():  cont.Addresses.Add(address)

    print cont

    ds.Models.Add(cont)

    contactdata.Position = recordend


def findContacts():

    # Contact database
    pbfile = "/pbm_master_v_20.dat"
    deletedfile = "/pbm_delete_file.dat"

    deletedcontacts = []


    # Parse the deleted contact file
    deletedcontactfile = ds.FileSystems[0].TryGetByPath(deletedfile)[1]
    if (deletedcontactfile):
        print "Parsing deleted contacts file: " + deletedfile

        deletedcontactdata = deletedcontactfile.Data
        deletedcontactdata.seek(0)

        # While we are not at the end of the file continue to get deleted contacts
        while deletedcontactdata.Length - deletedcontactdata.Position:
            deletedcontacts.append(readInt(deletedcontactdata))


    # Parse the contact file
    contactfile = ds.FileSystems[0].TryGetByPath(pbfile)[1]
    if (contactfile):
        print "Parsing contacts file: " + pbfile

        contactdata = contactfile.Data
        contactdata.seek(0)

        # While we are not at the end of the file continue to get contacts
        while contactdata.Length - contactdata.Position:
            parseContact(contactfile, contactdata, deletedcontacts)

#######################################################################################################
#
#   ZTE CALLS
#
#######################################################################################################
#
# CALL LOG FORMAT:
#
# FILE HEADER
# - Unknown UINT16LE
# - Unknown UINT16LE
# - File offset to end of records UINT32LE
# - Records start
#
# CALL RECORD FIELD FORMAT
# - Field identifier UNIT16LE - always 0x0E02
# - Field type number UINT16LE
# - Data type UINT16LE - valid values include ENUM (0x0C02), UINT16LE (0x1002), variable length UINT16LE/UINT32LE array (0x1005), STRING (0x1205)
# - Length of string or array types in bytes UINT16LE
# - Data
#
# KNOWN FIELD TYPES
# 1: Call type ENUM 1=Dialled, 2=Received, 3=Missed
# 4: Timestamp UINT32LE array - seconds since 6/1/1980 (GPS time) - multiple timestamps allowed
# 5: Duration UINT32LE array - duration in seconds - multiple allowed but correspond with timestamps
# 6: Phone number in ASCII
# 7: Unknown, UINT16LE array containing UTF-16 string usually 'v' but sometimes 'Unavailable'
# 8: Contact number UINT16LE or 0xFFFF if not in contacts
# 9: Phone number in UTF-16
# 16: Unknown UNIT16LE either 1 or 0 (Suspected read/unread flag)
# 17: Unknown UNIT16LE either 1 or 0 (Suspected read/unread flag)


def parseCall(callfile, calldata):

    partyIdentifierValue = None
    partyIdentifierSource = None
    calltimes = []
    durations = []

    recordID = readInt(calldata,2)
    recordsize = readInt(calldata,2)
    recordstart = calldata.Position

    if(recordID == 0xFFFF):         #       Seems to indicate a duplicate record not appearing in phone UI - skip it
        calldata.Position = recordstart + recordsize
        return

    # Continue to get fields until the end of the record
    while calldata.Position +2 < recordstart + recordsize:
        if not readInt(calldata,2) == 0x0E02:  # All valid fields begin with 0x0E02 - if bad skip it
            print "Error: bad field ID at offset: " + str(calldata.Position) + "/" + str(recordstart + recordsize)
            calldata.Position = recordstart + recordsize 
            return
        
        fieldID = readInt(calldata,2)

        if fieldID == 1:   # Call type
            if not readInt(calldata,2) == 0x0C02:   # Should be UNUM type
                calldata.Position = recordstart + recordsize
                print "Error: bad call type"
                return
            callType = readInt(calldata,2)
            if callType == 1: callTypeValue = CallType.Outgoing
            elif callType == 2: callTypeValue = CallType.Incoming
            elif callType == 3: callTypeValue = CallType.Missed
            else: call.Type.Value = CallType.Unknown
            callTypeSource = callfile.Data.GetSubRange(calldata.Position-8,8)

        elif fieldID == 4:  # Timestamp
            if not readInt(calldata,2) == 0x1005:   # Should be UINT32LE array type
                calldata.Position = recordstart + recordsize
                print "Error: bad timestamp"
                return
            size = readInt(calldata,2)
            callTimeStampSource = callfile.Data.GetSubRange(calldata.Position-8,8+size)
            if size % 4:    #  Must be a multiple of 4 bytes
                calldata.Position = recordstart + recordsize
                print "Error: bad timestamp"
                return
            # Get all the timestamps (add seconds between 6/1/1980 and 1/1/1970)
            for i in range(size/4): calltimes.append(readInt(calldata,4))#+315964800L
            
        elif fieldID == 5:  # Duration
            if not readInt(calldata,2) == 0x1005:   # Should be UINT32LE array type
                calldata.Position = recordstart + recordsize
                print "Error: bad duration"
                return
            size = readInt(calldata,2)
            callDurationSource = callfile.Data.GetSubRange(calldata.Position-8,8+size)
            if size % 4:    #  Must be a multiple of 4 bytes
                calldata.Position = recordstart + recordsize
                print "Error: bad duration"
                return
            for i in range (size/4): durations.append(readInt(calldata,4))
            
        elif fieldID == 6:  # Number in ASCII
            if not readInt(calldata,2) == 0x1205:
                calldata.Position = recordstart + recordsize
                print "Error: bad ASCII number"
                return
            if partyIdentifierValue != None:   # Prefer UTF-16 numbers
                readString(calldata,readInt(calldata,2))
                continue 
            numberpos = calldata.Position-6
            partyIdentifierValue = readString(calldata,readInt(calldata,2))
            partyIdentifierSource = callfile.Data.GetSubRange(numberpos,calldata.Position+1-numberpos)
            if callType == 1: partyRoleValue = PartyRole.To
            elif callType == 2 or callType == 3: partyRoleValue = PartyRole.From
            else: partyRoleValue = PartyRole.General
            
        elif fieldID == 9:  # Number in UTF-16
            if not readInt(calldata,2) == 0x1205:
                calldata.Position = recordstart + recordsize
                print "Error: bad UTF-16 number"
                return
            numberpos = calldata.Position-6
            partyIdentifierValue = readString(calldata,readInt(calldata,2), "utf-16")
            partyIdentifierSource = callfile.Data.GetSubRange(numberpos,calldata.Position-numberpos)
            if callType == 1: partyRoleValue = PartyRole.To
            elif callType == 2 or callType == 3: partyRoleValue = PartyRole.From
            else: partyRoleValue = PartyRole.General
            
        else:
            if not (fieldID == 7 or fieldID == 8 or fieldID == 16 or fieldID == 17):
                print "Unknown field type " + str(fieldID) + " at offset " + str(calldata.Position-4)
            dataType = readInt(calldata,2)
            if dataType == 0x1005:
                size = readInt(calldata,2)
                calldata.Position = calldata.Position+size
            elif dataType == 0x1002:
                data = readInt(calldata,2)
            else:
                calldata.Position = recordstart + recordsize
                print "Error: bad field 7"
                return

    party = Party()
    party.Identifier.Value = partyIdentifierValue
    party.Identifier.Source = partyIdentifierSource
    party.Role.Value = partyRoleValue

    if len(calltimes) == len(durations):
        for i in range(len(calltimes)):
            call = Call()
            call.Deleted = DeletedState.Intact
            call.Type.Value = callTypeValue
            call.Type.Source = callTypeSource
            call.Parties.Add(party)
            call.TimeStamp.Value = TimeStamp(DateTime(1980, 1, 6, 0, 0, 0).AddSeconds(calltimes[i]))
            call.TimeStamp.Source = callTimeStampSource
            call.Duration.Value = TimeSpan(0,0,durations[i])
            call.Duration.Source = callDurationSource
            ds.Models.Add(call)
            print call

    calldata.Position = recordstart+recordsize
        
def findCalls():

    fs = ds.FileSystems[0]
    for callfile in fs.Search('/shared/callhist$'):
        print "Parsing call log file: " + callfile.AbsolutePath

        calldata = callfile.Data
        calldata.seek(4)  # First 4 bytes are unknown (possibly version)

        dataend = readInt(calldata,4)  # Not sure what is after the end of the records

        # While we are not at the end of the file continue to get contacts
        while calldata.Position < dataend:
            parseCall(callfile, calldata)


#######################################################################################################
#
#   Entry point
#
#######################################################################################################

findContacts()
findMessages()
findCalls()
