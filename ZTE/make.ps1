﻿# Project Attributes
$author = "Trevor Welsby [ Welsby.TrevorR@police.qld.gov.au ]";
$name = "ZTE Contacts, Messages and Calls"
$desc = "Extract contact, message and call records from ZTE Cellebrite FileSystem extraction"
$entrypoint = "zte.py"
$file = "zte"

# Extract the version number from GIT
$version = iex "git describe --tags --long --dirty";

# Generate a versioned .tplug filename
$dest = "$($file)_v$($version.replace('.','_'))"

Write-Host "Generating $($pwd)\$($dest).tplug"

# Clear the bin directory
Write-Host "Clearing $($pwd)\"
Remove-Item –path .\bin -recurse
new-item .\bin -itemtype directory | out-null

# Copy in the source files
Write-Host "Copying source code files"
new-item .\bin\code -itemtype directory | out-null
Copy-Item -Path .\src\*.py -Destination .\bin\code\

# Create plugindefinition.xml
Write-Host "Generating plugindefinition.xml"

# Get a GUID for the plugin.zip file
$guid = (New-Guid).Guid.ToString();

# Create the file
$XmlWriter = New-Object XMl.XmlTextWriter("$($pwd.ToString())\bin\plugindefinition.xml",$Null)
$xmlWriter.Formatting = "Indented"
$xmlWriter.Indentation = "2"
 
# Write plugin Element
$xmlWriter.WriteStartElement("plugin")
$xmlWriter.WriteAttributeString("id", $guid)
$xmlWriter.WriteAttributeString("name", $name)
$xmlWriter.WriteAttributeString("type", "user")
$xmlWriter.WriteAttributeString("isBuiltIn", "False")
 
# Write metaData Element
$xmlWriter.WriteStartElement("metaData")

# Write Keys
$text= ,@("Description",$desc);
$text+=,@("MultiInputDumps","False");
$text+=,@("Author",$author);
$text+=,@("Version",$version);
$text+=,@("ScriptPath",$entrypoint);
$text+=,@("IsTestPlugin","False");

foreach($i in $text){
    $xmlWriter.WriteStartElement("key")
    $xmlWriter.WriteAttributeString("name", $i[0])
    $xmlWriter.WriteString($i[1])
    $xmlWriter.WriteEndElement()
}

# Close metaData Element
$xmlWriter.WriteEndElement()

# Write contract Element
$xmlWriter.WriteStartElement("contract")
$xmlWriter.WriteEndElement()

# Close plugin Element
$xmlWriter.WriteEndElement()
 
# Finish The Document
$xmlWriter.Finalize
$xmlWriter.Flush()
$xmlWriter.Close()

# Create the plugin.zip file
Write-Host "Creating plugin.zip"
Compress-Archive -Path .\bin\* -DestinationPath .\bin\plugin.zip
Remove-Item –path .\bin\code -recurse
Remove-Item –path .\bin\plugindefinition.xml

# Create plugin.xml
Write-Host "Generating plugin.xml"

# Get a GUID for the .tplug file
$guid = (New-Guid).Guid.ToString();

# Create the file
$XmlWriter = New-Object XMl.XmlTextWriter("$($pwd.ToString())\bin\plugin.xml",$Null)
$xmlWriter.Formatting = "Indented"
$xmlWriter.Indentation = "2"
 
# Write plugin Element
$xmlWriter.WriteStartElement("plugin")
$xmlWriter.WriteElementString("author", $author)
$xmlWriter.WriteElementString("version", $version)
$xmlWriter.WriteElementString("guid", $guid)
$xmlWriter.WriteElementString("name", $name)
$xmlWriter.WriteEndElement()
 
# Finish The Document
$xmlWriter.Finalize
$xmlWriter.Flush()
$xmlWriter.Close()

# Create the .tplug file
Write-Host "Creating $($dest).tplug"
Compress-Archive -Path .\bin\* -DestinationPath ".\bin\$($dest).zip"
Rename-Item -Path ".\bin\$($dest).zip" -NewName "$($dest).tplug"

# Cleanup temp files
Write-Host "Cleaning up"
Remove-Item –path .\bin\plugin.zip
Remove-Item –path .\bin\plugin.xml

Write-Host "$($dest).tplug successufully created"
